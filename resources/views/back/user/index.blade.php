@extends('back.layout', ['parent' => 'user'])
@section('title', 'Users')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Users</h2>
        <ol class="breadcrumb page-head-nav">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li class="active"><a>Users</a></li>
        </ol>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-body">
                        <table id="users" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Avatar</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Username</th>
                                <th>E-mail</th>
                                <th>Gender</th>
                                <th>User Level</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td><img src="{{ url('img/back/avatar.png') }}" class="img-circle" style="max-width:50px" /></td>
                                <td>{{ $user->firstname }}</td>
                                <td>{{ $user->lastname }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->gender }}</td>
                                <td>{{ $user->user_level }}</td>
                                <td>
                                <div class="btn-group btn-space">
                                    <a href="{{ url('user/'.$user->id.'/edit') }}" class="btn btn-primary"><i class="icon mdi mdi-edit"></i></a>
                                    <a data-modal="ban-mod" data-user-id="{{ $user->id }}" data-avatar-url="{{ $user->avatar }}" data-user-username="{{ $user->username }}" class="btn btn-warning md-trigger"><i class="icon mdi mdi-block"></i></a>
                                    <a data-modal="del-mod" data-user-id="{{ $user->id }}" data-avatar-url="{{ $user->avatar }}" data-user-username="{{ $user->username }}" class="btn btn-space btn-danger md-trigger"><i class="icon mdi mdi-close"></i></a>
                                </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="del-mod" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="modal-main-icon" style="display: inline">
                        <img class="img-circle" src="{{ url('img/back/avatar.png') }}" style="vertical-align: baseline; max-width: 55px" />
                        <span class="mdi mdi-arrow-forward"></span>
                        <span class="mdi mdi-delete-forever"></span>
                    </div>
                    <h3>Attention!</h3>
                    <p>Are you sure you want to delete the user: <b><span id="user_name"></span></b>.<br>By pressing the "Delete" button, it will be impossible to go back.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Delete</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
    <div id="ban-mod" class="modal-container modal-full-color modal-full-color-warning modal-effect-8">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="modal-main-icon" style="display: inline; margin-left:30px">
                        <img class="img-circle" src="{{ url('img/back/avatar.png') }}" style="vertical-align: baseline; max-width: 55px" />
                        <span class="mdi mdi-arrow-forward"></span>
                        <img class="img-circle" src="{{ url('img/back/avatar.png') }}" style="vertical-align: baseline; max-width: 55px" />
                        <span class="mdi mdi-block" style="font-size: 30px; top: 10px; right: 30px;"></span>
                    </div>
                    <h3>Attention!</h3>
                    <p>Are you sure you want to ban the user: <b><span id="user_name"></span></b>.<br>By pressing the "Ban" button, it will be impossible to go back.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                        <input type="hidden" id="user_id" name="user_id" value="">
                        <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Ban</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ url('lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            App.init();
        });
        $.extend( true, $.fn.dataTable.defaults, {
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        } );

        $("#users").dataTable({
            pageLength: 50,
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            dom:  "<'row be-datatable-header'<'col-sm-4'l><'col-sm-4'f><'col-sm-4 text-right'B>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });


        $.fn.niftyModal('setDefaults',{
            overlaySelector: '.modal-overlay',
            closeSelector: '.modal-close',
            classAddAfterOpen: 'modal-show',
        });
        // Click on ban button
        $('.btn-warning.md-trigger').click(function(){
            var username = $(this).attr('data-user-username');
            var user_id = $(this).attr('data-user-id');
            var avatar_url = $(this).attr('data-avatar-url');
            $('#ban-mod span#user_name').html(username);
            $('#ban-mod input#user_id').val(user_id);
        });

        // Click on del button
        $('.btn-danger.md-trigger').click(function(){
            var username = $(this).attr('data-user-username');
            var user_id = $(this).attr('data-user-id');
            var avatar_url = $(this).attr('data-avatar-url');
            $('#del-mod span#user_name').html(username);
            $('#del-mod input#user_id').val(user_id);
        });
    </script>
@endsection