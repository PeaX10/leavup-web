<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-touch-icon.png?v=M4opLavdag">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png?v=M4opLavdag">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png?v=M4opLavdag">
    <link rel="manifest" href="img/icons/manifest.json?v=M4opLavdag">
    <link rel="mask-icon" href="img/icons/safari-pinned-tab.svg?v=M4opLavdag" color="#ff5e3a">
    <link rel="shortcut icon" href="img/icons/favicon.ico?v=M4opLavdag">
    <title>LeavUP - Back Office</title>
    <link rel="stylesheet" type="text/css" href="{{ url('lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ url('css/back/style.css') }}" type="text/css"/>
</head>
<body class="be-splash-screen">
<div class="be-wrapper be-login">
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="splash-container">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading"><img src="{{ url('img/back/logo-xx.png') }}" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Please enter your user information.</span></div>
                    <div class="panel-body">
                        <form action="#" method="get">
                            <div class="form-group">
                                <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" placeholder="Password" class="form-control">
                            </div>
                            <div class="form-group row login-tools">
                                <div class="col-xs-6 login-remember">
                                    <div class="be-checkbox">
                                        <input type="checkbox" id="remember">
                                        <label for="remember">Remember Me</label>
                                    </div>
                                </div>
                                <div class="col-xs-6 login-forgot-password"><a href="#">Forgot Password?</a></div>
                            </div>
                            <div class="form-group login-submit">
                                <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Sign me in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('lib/jquery/jquery.js') }}" type="text/javascript"></script>
<script src="{{ url('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/back/main.js') }}" type="text/javascript"></script>
<script src="{{ url('lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
    });

</script>
</body>
</html>