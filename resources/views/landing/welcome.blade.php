<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>LeavUP - Connecter vous à la vie réelle</title>
    <meta name="description" content="LeavUP vous permet de sortir et de rencontrer des personnes non loin de chez vous, en pratiquant diverses activités.">
    <meta name="keywords" content="Sortir, Rencontre, LeavUP, Sport, Loisirs">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-touch-icon.png?v=M4opLavdag">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png?v=M4opLavdag">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png?v=M4opLavdag">
    <link rel="manifest" href="img/icons/manifest.json?v=M4opLavdag">
    <link rel="mask-icon" href="img/icons/safari-pinned-tab.svg?v=M4opLavdag" color="#ff5e3a">
    <link rel="shortcut icon" href="img/icons/favicon.ico?v=M4opLavdag">
    <meta name="apple-mobile-web-app-title" content="LeavUP">
    <meta name="application-name" content="LeavUP">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="{{ url('css/custom-animations.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/lib/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}" />

    <!--[if lt IE 9]>
    <script src="{{ url('js/html5shiv.js') }}"></script>
    <script src="{{ url('js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body id="color-landing-page" class="color-landing-page">
<!-- Preloader -->
<div class="preloader-mask">
    <div class="preloader"><div class="spin base_clr_brd"><div class="clip left"><div class="circle"></div></div><div class="gap"><div class="circle"></div></div><div class="clip right"><div class="circle"></div></div></div></div>
</div>

<!-- Header -->
<header>
    <nav class="navigation navigation-header white-dropdown">
        <div class="container">
            <div class="navigation-brand">
                <div class="brand-logo">
                    <a href="{{ url('/') }}" class="logo"></a><a href="{{ url('/') }}" class="logo logo-alt"></a>
                </div>
            </div>
            <button class="navigation-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navigation-navbar collapsed">
                <!--
                <ul class="navigation-bar navigation-bar-left">
                    <li><a href="#hero">Accueil</a></li>
                    <li><a href="#about">C'est Quoi ?</a></li>
                    <li><a href="#features">Fonctionnalités</a></li>
                    <li><a href="#contact">Contactez-nous</a></li>
                </ul>
                -->
                <ul class="navigation-bar navigation-bar-right">
                    <li class="featured">
                        <div class="dropdown">
                            <button class="beta-register btn btn-sm btn-outline dropdown-toggle" type="button" id="languageMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {{ strtoupper(App::getLocale()) }} <img src="{{ url('img/flags/'.App::getLocale().'.png') }}" alt="Langage {{ App::getLocale() }}"/>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="languageMenu">
                                @foreach(config('localization.available_locales') as $lang)
                                    @if(App::getLocale() != $lang)<li><a href="https://{{$lang}}.leavup.com{{ Request::path() }}">{{ $lang }} <img src="{{ url('img/flags/'.$lang.'.png') }}" alt="Langage {{ $lang }}"/></a></li>@endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    <!-- <li class="featured"><a href="{{ url('register/beta') }}" class="beta-register btn btn-sm btn-outline">Devenir <i class="fa fa-bold"></i>-testeur ?</a></li> -->
                </ul>
            </div>
        </div>
    </nav>
</header>

<div id="hero" class="bg bg1 static-header window-height light-text hero-section ytp-player-background clearfix" data-video="94YUYp8baWE" data-property="{videoURL: 'https://www.youtube.com/watch?v=94YUYp8baWE', containment: '#hero', autoPlay: true, realfullscreen: false, stopMovieOnBlur: false, addRaster: false, showControls: false, mute:true, startAt:0, opacity:0.4, gaTrack: false}">
    <div class="heading-bdlock align-center centered-block">
        <div class="container">
            <h3 class="editContent" style="text-transform: uppercase;">La vie est trop courte pour des sorties ordinaires.</h3>
            <h5 class="editContent">Inscris-toi pour tester LeavUP en premier et faire des <span class="highlight">sorties extraordinaires.</span></h5>
            <form class="form subscribe-form" style="padding-top: 10px;" action="{{ url('/be_aware') }}" method="post">
                <div class="form-group form-inline">
                    {{ csrf_field() }}
                    <input size="35" type="email" class="form-control required" name="email" placeholder="Ton E-mail" />
                    <input type="submit" class="btn btn-solid" value="JE VEUX TESTER" />
                </div>
            </form>
        </div>
    </div>
</div>


<section id="about" class="section about-section align-center dark-text">
    <div class="container" style="font-size: 22px">
        <div style="text-align: justify" class="col-md-offset-3 col-md-6">
            <p>Nous aimons tous sortir</p>
            <p>Mais soyons honnêtes, nous sortons toujours aux mêmes endroits, avec les mêmes personnes.</p>
            <p>Chez LeavUp, nous croyons dur comme fer qu'il est temps de sortir pour de vrai.</p>
            <p>En ce moment même, nous sommes en train de construire le futur des sorties. Un réseau social innovant, mobile et local. Où vous pourrez créer ou rejoindre les meilleurs plans dans votre ville et ses environs. Rien de mieux pour s’amuser en famille, entre amis ou pour faire de nouvelles rencontres.</p>
            <p>Et ça arrive bientôt sur LeavUP.</p>
        </div>
        <div class="smartphone col-md-offset-3 col-md-6" style="color: #dddddd;">
            <h4>Bientôt sur</h4>
            <div style="font-size:40px" class="col-md-6 col-md-offset-3">
                <div class="col-xs-4">
                    <i class="fa fa-apple"></i>
                </div>
                <div class="col-xs-4">
                    <i class="fa fa-android"></i>
                </div>
                <div class="col-xs-4">
                    <i class="fa fa-desktop"></i>
                </div>
            </div>
        </div>
    </div>
</section>

<footer id="footer" class="footer light-text">
    <div class="container">
        <div class="footer-content row">
            <div class="col-sm-3 col-xs-12">
                <div class="logo-wrapper">
                    <a href="{{ url('/') }}"><img width="130" height="31" src="{{ url('img/logo-white.png') }}" alt="logo" /></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <ul class="list-inline menu">
                    <li><a href="{{ url('contact') }}" alt="LeavUP - Contactez-nous">Contact</a></li>
                    <li><a>© 2017 LeavUP</a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <ul class="list-inline social" style="font-size:22px">
                    <li><a class="btn-rounded" href="https://facebook.com/LeavUP.FR"><span class="fa fa-facebook"></span></a></li>
                    <li><a class="btn-rounded" href="https://twitter.com/LeavUP_com"><span class="fa fa-twitter"></span></a></li>
                    <li><a class="btn-rounded" href="https://www.instagram.com/leavup_com"><span class="fa fa-instagram"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

<!--[if lt IE 9]>
<script type="text/javascript" src="{{ url('js/jquery-1.11.3.min.js?ver=1') }}"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script type="text/javascript" src="{{ url('js/jquery-2.1.4.min.js?ver=1') }}"></script>
<!--<![endif]-->

<script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.flexslider-min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.appear.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.plugin.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.countdown.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.mb.YTPlayer.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery-ui-slider.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/startuply.js') }}"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        toastr.options = {
            positionClass: 'toast-bottom-right'
        };
        @if(Request::session()->has('be_aware_success'))
            toastr.success('Merci! On vous tient au courant de la sortie, promis.');
        @elseif(Request::session()->has('be_aware_already_exist'))
            toastr.error('Oupps! Cette adresse mail a déjà été enregistré!');
        @elseif(Request::session()->has('be_aware_error'))
            toastr.error('Oupps! Merci d\'entrer une adresse mail correcte!');
        @elseif(Request::session()->has('contact_message_success'))
            toastr.success('Ton message a bien été envoyé !');
        @endif
    });
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-99741992-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
