<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Available locales
    |--------------------------------------------------------------------------
    |
    | An array of the locales accepted by the routing system.
    |
    */
    'available_locales' => ['en', 'fr', 'ar'],

    /*
    |--------------------------------------------------------------------------
    | Available locales
    |--------------------------------------------------------------------------
    |
    | Use this option to enable or disable the use of cookies
    | in locale detection.
    |
    */
    'cookie_localization' => true,

    /*
    |--------------------------------------------------------------------------
    | Available locales
    |--------------------------------------------------------------------------
    |
    | Use this option to enable or disable the use of the browser settings
    | in locale detection.
    |
    */
    'browser_localization' => true,

    /*
    |--------------------------------------------------------------------------
    | Available locales
    |--------------------------------------------------------------------------
    |
    | Here you may change the name of the cookie used to save the locale.
    | This option is used only if localization with cookie is enabled.
    |
    */
    'cookie_name' => 'locale',

];