<?php

namespace App\Http\Controllers\BackOffice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        if(!Auth::check()){
            return abort(404);
        }else{
            $stats = ['users', 'events'];
            $date = new \DateTime('tomorrow -1 week');

            // Get number of subcriptions by days
            $stats['users'] = User::select(array(
                DB::raw('DATE(`created_at`) as `date`'),
                DB::raw('COUNT(*) as `count`')
            ))
                ->where('created_at', '>', $date)
                ->groupBy('date')
                ->orderBy('date', 'DESC')
                ->pluck('count', 'date')
                ->toArray();

            $interval = $this->generateDateRange(Carbon::parse('tomorrow -1 week'), Carbon::now());
            $stats['nb_users'] = 0;
            $stats['spark_users'] = '';
            foreach($interval as $key => $value){
                if(key_exists($value, $stats['users'])){
                    $stats['nb_users'] += $stats['users'][$value];
                    $stats['spark_users'] .= $stats['users'][$value].',';
                }else{
                    $stats['spark_users'] .= '0,';
                }
            }
            $stats['spark_users'] = substr($stats['spark_users'], 0, -1);
            return view('back.index', compact('stats'));
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

}
