<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\BeAware;
use App;
use App\ContactMessage;

class LandingController extends Controller
{
    public function be_aware(Request $request){
        $rules = ['email' => 'required|email|unique:be_awares'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            if(BeAware::where('email', Input::get('email'))->exists()){
                return redirect('/')->with('be_aware_already_exist', 'true');
            }else{
                return redirect('/')->with('be_aware_error', 'true');
            }
        }else{
            $user = new BeAware();
            $user->email = Input::get('email');
            $user->lang = App::getLocale();
            $user->save();
            return redirect('/')->with('be_aware_success', 'true');
        }
    }

    public function contact(){
        return view('landing.contact');
    }

    public function sendContact(Request $request){
        $rules = [  'email' => 'required|email',
                    'name' => 'required|min:3',
                    'message' => 'required|min:30'

        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }else{
            $contact = new ContactMessage();
            $contact->name = Input::get('name');
            $contact->email = Input::get('email');
            $contact->message = Input::get('message');
            $contact->ip = $request->ip();
            $contact->save();

            return redirect('/')->with('contact_message_success', 'true');
        }
    }
}
