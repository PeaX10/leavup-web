<?php

namespace App\Http\Middleware;

use Closure;
use App;

class SubdomainRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // FORCE HTTPS AND REDIRECT TO SUBDOMAIN LANGUAGE
        $authorized_languages = config('localization.available_locales');
        $pieces = explode('.', $request->getHost());
        if(!in_array($pieces[0], $authorized_languages) && $pieces[0] != 'bo'){
            return redirect('https://'.App::getLocale().'.leavup.com');
        }

        return $next($request);
    }
}
