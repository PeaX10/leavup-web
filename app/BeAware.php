<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeAware extends Model
{
    protected $fillable = [
        'name', 'email', 'lang'
    ];
}
