<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use App\User;

# Back
Route::group(['domain' => 'bo.'.env('APP_DOMAIN')], function () {
    Route::get('/', 'BackOffice\HomeController@index');
    Route::resource('user', 'BackOffice\UserController');
    Route::get('/logout', 'BackOffice\HomeController@logout');
    Route::get('/login/alex', function(){
        Auth::login(User::find(1), true);
        return redirect('/');
    });
});


// Landing Page
Route::get('/', function () {
    return view('landing.welcome');
});

Route::get('/contact', 'LandingController@contact');
Route::post('/contact', 'LandingController@sendContact');
Route::post('/be_aware', 'LandingController@be_aware')->name('be_aware');
// END Landing Page